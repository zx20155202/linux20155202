#include <sys/types.h>
#include <unistd.h>

pid_t fork(void);
void main()
{
    printf("0 \n ");
    fork();
    printf("1 \n");
    fork();
    printf("__\n ");
}
