int main(){  
    int childpid;  
    int i;  
  
    if (fork() == 0){  
        //child process  
        for (i=1; i<=8; i++){  
            printf("This is child process\n");  
        }  
    }else{  
        //parent process  
        for(i=1; i<=8; i++){  
            printf("This is parent process\n");  
        }  
    }  
  
    printf("step2 after fork() !!\n\n");  
}  
