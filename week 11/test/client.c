#include <sys/types.h>  
#include <sys/socket.h>  
#include <netinet/in.h>  
#include <arpa/inet.h>  
#include <stdio.h>
#define MY_PORT 155202

int main() {

    char buffer1[1024]="connect to server failed!";
    char *buffer2;
    char buffer3[1024]="\0";
    char buffer4[1024]="\0";
    int n;
    int bytes_recvd;

    //WSADATA wsaData;
    //WSAStartup(MAKEWORD(1,1),&wsaData);

    struct sockaddr_in remote_addr;
	remote_addr.sin_family = AF_INET;
	remote_addr.sin_port = htons(MY_PORT);
	remote_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	int conn_sock,new_sock;

	conn_sock = socket(AF_INET, SOCK_STREAM, 0);
	connect(conn_sock, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));

	 bytes_recvd = recv(conn_sock, buffer1, sizeof(buffer1), 0);
     printf("Client received message(%d bytes): %s\n", bytes_recvd, buffer1);

     buffer2="client connected to server successful!";
     send(conn_sock, buffer2, strlen(buffer2), 0);
     //new_sock:用于传输数据的socket描述符,buffer:是一个指向要发送数据的指针,strlen(buffer):以字节为单位的数据的长度。

     for(n=0;n<1024;n++)
     {
         memset(buffer3, 0, sizeof(buffer3));
         printf("client:");
         gets(buffer3);
         send(conn_sock, buffer3, strlen(buffer3), 0);

         memset(buffer4, 0, sizeof(buffer4));
         bytes_recvd = recv(conn_sock, buffer4, sizeof(buffer4), 0);
         printf("server:%s\n", buffer4);
     }
	close(new_sock);
	close(conn_sock);
	//WSACleanup();

	return 0;
}
