#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>  
#include <sys/socket.h>  
#include <netinet/in.h>  
#include <arpa/inet.h>
#define MY_PORT 3434

int main()
{
    char *buffer1="connect to server successful!";
    char buffer2[1024]="no client connected to server!";
    char buffer3[1024]="\0";
    char buffer4[1024]="\0";
    int bytes_recvd;
    int n;

    //WSADATA wsaData;
    //WSAStartup(MAKEWORD(1,1),&wsaData);

    struct sockaddr_in my_addr;
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(MY_PORT);//htons:将主机的无符号短整型数字节顺序转换成网络字节顺序
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY);//htonl:将主机的无符合长整型数字节顺序转换成网络字节顺序。

    int listen_sock, new_sock;
    listen_sock = socket(AF_INET, SOCK_STREAM, 0);
    //根据指定的地址族、数据类型和协议来生成一个套接字的描述字(listen_sock )
    //地址描述:AP_INET,指定socket类型：SOCK_STREAM,函数返回值为整型socket描述符。
    bind(listen_sock, (struct sockaddr *)&my_addr, sizeof(struct sockaddr));//将一个套接字和一个本地地址与绑定在一起
    //listen_sock:socket描述符,(struct sockaddr *)&my_addr:指向sockaddr类型的指针
    listen(listen_sock, 5);
    //5:在请求队列中允许的最大请求数为5

    while(1)
    {
        new_sock = accept(listen_sock, NULL, NULL);
        //listen_sock:被监听的socket描述符

        send(new_sock, buffer1, strlen(buffer1), 0);
        //new_sock:用于传输数据的socket描述符,buffer:是一个指向要发送数据的指针,strlen(buffer):以字节为单位的数据的长度。

        bytes_recvd = recv(new_sock, buffer2, sizeof(buffer2), 0);
        printf("Server received message(%d bytes): %s\n", bytes_recvd, buffer2);

        for(n=0;n<1024;n++)
        {
            memset(buffer3, 0, sizeof(buffer3));
            bytes_recvd = recv(new_sock, buffer3, sizeof(buffer3), 0);
            printf("client:%s\n",buffer3);

            memset(buffer4, 0, sizeof(buffer4));
            printf("server:");
            gets(buffer4);
            send(new_sock, buffer4, strlen(buffer4), 0);
        }

    }
    close(new_sock);
    close(listen_sock);

    //WSACleanup();

    return 0;
}

